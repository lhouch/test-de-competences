//
//  TasksViewController.swift
//  MAChalange
//
//  Created by lhouch mohamed on 02/15/22.
//  Copyright © 1400 AP lhouch mohamed. All rights reserved.
//

import UIKit

class TasksViewController: UITableViewController {

    var tasks:[Task]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.reloadData()
    }
    
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tasks?.count ?? 0
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CountryCell", for: indexPath)
        if let task = tasks?[indexPath.row]{
            cell.textLabel?.numberOfLines = 0
            cell.textLabel?.text = task.title
            cell.detailTextLabel?.text = (task.completed ?? false ? "✅" : "☑️")
        }
        return cell
    }
    

}
