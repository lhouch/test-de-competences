//
//  ViewController.swift
//  MAChalange
//
//  Created by lhouch mohamed on 02/15/22.
//  Copyright © 2019 lhouch mohamed. All rights reserved.
//

import UIKit


class MainViewController: UITableViewController  {
    
    
    var listUsers:[User]? {
        didSet {
            self.tableView.reloadData()
        }
    }
    
    var indexSelected = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        getUsers()
        
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listUsers?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CountryCell", for: indexPath)
        if let user = listUsers?[indexPath.row]{
            cell.accessoryType = .disclosureIndicator
            cell.textLabel?.text = user.name
            cell.detailTextLabel?.text = user.email
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let user = listUsers?[indexPath.row]{
            self.indexSelected = indexPath.row
            self.getTasksByUser(user)
        }
        
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let tasksViewController = segue.destination as? TasksViewController {
            tasksViewController.tasks = self.listUsers?[self.indexSelected].tasks
        }
    }
}


extension MainViewController{
    
    func getUsers() {
        self.showActivityIndicatoryInSuperview()

        if UserDefaults.standard.isKeyPresentInUserDefaults(key: UserDefaultsKeys.users.rawValue) {
            self.listUsers = UserDefaults.standard.getValueUsers()
        }
        
        WService.sharedInstance.getUsersList{ (res) in
            switch res {
            case .success(let users):
                
                DispatchQueue.main.async {
                    
                    UserDefaults.standard.setValueUsers(users: users)
                    self.hideActivityIndicatoryInSuperview()
                    self.listUsers = users
                    
                }
                
                
            case .failure(let err):
                
                DispatchQueue.main.async {
                    self.hideActivityIndicatoryInSuperview()
                    if !UserDefaults.standard.isKeyPresentInUserDefaults(key: UserDefaultsKeys.users.rawValue) {
                        self.showAlertViewInSuperview("Failed to fetch users", message: err.localizedDescription, completion: {
                        })
                    }
                   
                }
                
            }
        }
    }
    
    func getTasksByUser(_ user: User){
        self.showActivityIndicatoryInSuperview()
        WService.sharedInstance.getTasksListByUser(userId: user.id!) { (res) in
            switch res {
            case .success(let tasks):
                
                DispatchQueue.main.async {
                    self.hideActivityIndicatoryInSuperview()
                    UserDefaults.standard.setValueTasks(tasks: tasks, userId: user.id!)
                    self.listUsers?[self.indexSelected].tasks = tasks
                    self.performSegue(withIdentifier: "showDetail", sender: self)

                }
                
                
            case .failure(let err):
                DispatchQueue.main.async {
                    self.hideActivityIndicatoryInSuperview()
                    if let tasks = self.listUsers?[self.indexSelected].tasks, !tasks.isEmpty {
                        self.performSegue(withIdentifier: "showDetail", sender: self)
                    } else {
                        self.showAlertViewInSuperview("Failed to fetch tasks", message: err.localizedDescription, completion: {
                        })
                    }
                    
                }
            }
        }
    }
    
    
    
}
