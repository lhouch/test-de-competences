//
//  UserDefaults.swift
//  MAChalange
//
//  Created by lhouch mohamed on 02/15/22.
//  Copyright © 2019 lhouch mohamed. All rights reserved.
//

import UIKit

extension UserDefaults  {
    
    func isKeyPresentInUserDefaults(key: String) -> Bool {
        return UserDefaults.standard.object(forKey: key) != nil
    }
    
    //MARK: Retrieve users
    func getValueUsers() -> [User] {
        
        var mData:[User]
        if let data = UserDefaults.standard.value(forKey: UserDefaultsKeys.users.rawValue) as? Data {
            let datas = try? PropertyListDecoder().decode([User].self, from: data)
            mData = datas! as [User]
        }else
        {
            mData = [User]()
        }
        return mData
    }
    
    //MARK: save users
    func setValueUsers(users: [User]) {
        if let encoded = try? PropertyListEncoder().encode(users){
                UserDefaults.standard.set(encoded, forKey: UserDefaultsKeys.users.rawValue)
        }
    }
    
    
    //MARK: Retrieve tasks
    func getValueTasks() -> String {
        return UserDefaults.standard.string(forKey: UserDefaultsKeys.users.rawValue)!
    }

    //MARK: save tasks
    func setValueTasks(tasks: [Task], userId: Int){
        var mData:[User]
        if let data = UserDefaults.standard.value(forKey: UserDefaultsKeys.users.rawValue) as? Data {
            let datas = try? PropertyListDecoder().decode([User].self, from: data)
            mData = datas! as [User]
            if let row = mData.firstIndex(where: {$0.id == userId}) {
                mData[row].tasks = tasks
            }
            setValueUsers(users: mData)
            
        }
    }
    
    
    func findUserById(userID : Int) -> User {
        var mData:[User]
        var user : User = User()
        if let data = UserDefaults.standard.value(forKey: UserDefaultsKeys.users.rawValue) as? Data {
            let datas = try? PropertyListDecoder().decode([User].self, from: data)
            mData = datas! as [User]
            if let row = mData.firstIndex(where: {$0.id == userID}) {
                user = mData[row]
            }
        }
        return user
    }
    
}
