//
//  Global.swift
//  MAChalange
//
//  Created by lhouch mohamed on 02/15/22.
//  Copyright © 2019 lhouch mohamed. All rights reserved.
//

import UIKit

/*****************API Key***********************/
let BASE_URL = "https://jsonplaceholder.typicode.com/"
let  List_USERS_URL = BASE_URL + "users/"
let  TASKS_USER_URL = BASE_URL + "todos?userId="



/*****************Object Key***********************/
var ActivityIndicatorViewInSuperviewAssociatedObjectKey = "_UIViewActivityIndicatorViewInSuperviewAssociatedObjectKey";

/*****************User Defaults Key***********************/
enum UserDefaultsKeys : String {
    case users
}
