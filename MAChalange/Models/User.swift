//
//  User.swift
//  MAChalange
//
//  Created by lhouch mohamed on 02/15/22.
//  Copyright © 1400 AP lhouch mohamed. All rights reserved.
//

struct User : Codable {
    var id : Int?
    var name : String?
    var email : String?
    var tasks: [Task]?
}

