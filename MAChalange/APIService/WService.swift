//
//  WService.swift
//  MAChalange
//
//  Created by lhouch mohamed on 02/15/22.
//  Copyright © 2019 lhouch mohamed. All rights reserved.
//

import Foundation

class WService: NSObject {
    
    static let sharedInstance = WService()
    
    
    func getUsersList(completion: @escaping (Result<[User], Error>) -> ()) {
        guard let url_ = URL(string: List_USERS_URL) else { return }
        URLSession.shared.dataTask(with: url_) { (data, resp, err) in
            // if is error response
            if let err = err {
                completion(.failure(err))
                return
            }
            // if is successful response
            do {
                let users = try JSONDecoder().decode([User].self, from: data!)
                completion(.success(users))
                
            } catch let jsonError {
                completion(.failure(jsonError))
            }
        }.resume()
    }
    
    
    func getTasksListByUser(userId: Int, completion: @escaping (Result<[Task], Error>) -> ()) {
        
        guard let url_ = URL(string: "\(TASKS_USER_URL)\(userId)") else { return }
        URLSession.shared.dataTask(with: url_) { (data, resp, err) in
            // if is error response
            if let err = err {
                completion(.failure(err))
                return
            }
            // if is successful response
            do {
                let tasks = try JSONDecoder().decode([Task].self, from: data!)
                completion(.success(tasks))
                
            } catch let jsonError {
                completion(.failure(jsonError))
            }
        }.resume()
    }
}

